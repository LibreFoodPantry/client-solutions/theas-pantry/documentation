# Thea's Pantry

![Thea's Pantry](TheasPantry.png)

[Thea's Pantry](https://www.worcester.edu/Theas-Pantry/) - A food pantry
for the Worcester State University community.

## Documentation

* [Developer Documentation](Developer/README.md) - How to contribute to the
  development of Thea's Pantry including community standards, tools and
  processes, and design decisions.
* [Customer Documentation](Customer/README.md) - Examples from the customer's
current process.
* [User Documentation](User/README.md) - Coming Soon
* Licenses - We license all our code under [GPLv3](LICENSES/GPL-3.0-only.txt)
and all other content under [CC-BY-SA 4.0](LICENSES/CC-BY-SA-4.0.txt).

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
