# Pipelines

Most of Thea's Pantry projects have GitLab pipelines configured, using
the [LibreFoodPantry pipeline tools](https://gitlab.com/LibreFoodPantry/common-services/tools).

* Default branches will build, lint, test, and release the project's product
when a merge commit is made.
* Feature branches will build, lint, and test on pushes.

Read the [Pipeline Documentation](https://gitlab.com/LibreFoodPantry/common-services/tools/documentation)
to learn how to configure a project's `.gitlab-ci.yaml` file to provide
the appropriate pipeline jobs for a project.

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
