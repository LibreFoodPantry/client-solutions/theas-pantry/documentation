# Wireframes for Frontend GUIs

## GuestInfoSystem

### GuestInfoFrontend - Rename to CheckinGuestFrontend?

```plantuml
@startsalt
{^"Guest Checkin"
WSU ID | "0123456"
[Cancel] | [Check In]
}
@endsalt
```

```plantuml
@startsalt
{^"Guest Checkin"
Zipcode | "01602"
WSU ID | "0123456"
[X] On-Campus Resident
[ ] Receiving Unemployment Benefits
{^"Assistance Received"
[ ] Social Security
[ ] TANF
[X] Financial Aid
[ ] SNAP
[ ] WIC
[ ] Free Breakfast
[ ] Free Lunch
[ ] SFSP
[ ] Other
}
[Cancel] | [Submit]
}
@endsalt
```

## InventorySystem

### AddInventoryFrontend

```plantuml
@startsalt
{^"Add Inventory"
Pounds added | "500"
WSU ID | "0123456"
[Cancel] | [Submit]
}
@endsalt
```

```plantuml
@startsalt
{^"Add Inventory Confirm"
Pounds added | "500"
Net pounds | "1500"
WSU ID | "0123456"
[Cancel] | [Confirm]
}
@endsalt
```

### CheckInventoryFrontend

```plantuml
@startsalt
{^"Check Inventory"
[Check Inventory]
}
@endsalt
```

```plantuml
@startsalt
{^"Current Inventory"
250 pounds
[Exit]
}
@endsalt
```

### CheckoutGuestFrontend

```plantuml
@startsalt
{^"Guest Checkout"
WSU ID | "0123456"
Pounds taken | "25"
[Cancel] | [Check Out]
}
@endsalt
```

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
