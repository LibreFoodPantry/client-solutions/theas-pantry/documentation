# Development Environment

Thea's Pantry suggests the use of [Gitpod](https://www.gitpod.io/) for your
development environment.

For most developers working in Thea's Pantry, the free credits provided by
Gitpod will be sufficient. Additional credits, if needed, are inexpensive.

* Developers will need a free [Gitpod account](https://gitpod.io/login).
* We suggest installing the [Gitpod browser extension](https://www.gitpod.io/docs/configure/user-settings/browser-extension)
to make it easier to open projects in Gitpod.
* We suggest using the VSCode IDE in Gitpod and have configured most of our
  projects to support this development environment.
* When opening a project in Gitpod, you may notice that it takes a while
  for the recommended VSCode extensions to be installed into the workspace.
  As long as you do not delete your workspace the extensions will remain
  installed for the next time you open it.
* You may want to pin commonly used workspaces in your Gitpod account so
  that they will not be automatically deleted after 14 days of inactivity.
* Be sure to stop Gitpod workspaces when you are not working. This will
  help save your free credits. You can stop your workspace for the terminal
  with the `gp stop` command.

Most of our projects also have VSCode Dev Containers available, but we
have found this to be challenging for some of our developers due to issues
with installing and running Docker, especially on machines with limited
resources.

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
