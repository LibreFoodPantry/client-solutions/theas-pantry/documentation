# Linting

We use linters to ensure that our code and documentation meets standards,
and to do so automatically without developers having to expend precious
time and effort to check files.

Files are checked by our linters in three ways: in a VSCode extension, by
running them locally in our development environment, and by running them in
our pipelines whenever we push changes to GitLab.

## Available Linters

LibreFoodPantry has a collection of linters available to run as Docker
images. They can be seen [here](https://gitlab.com/LibreFoodPantry/common-services/tools/linters).

Developers should review the types of files in the project they are working
on and use all of available linters that are appropriate to the file types.

Each of the linters has a README file that:

* Explains what the linter is used for
* Links to the linter's website and documentation
* Explains how to enable the linter in a project's pipeline.
* Explains how to use it in VSCode (if an extension is available.)
* Explains how to run it locally at the command line, or to include it in bash script.
* Explains how to configure the linter.

## Running as a VSCode extension

If there is a VSCode extension for the linter, it should be added to the
`.vscode/extensions.json` file, and any appropriate settings added to the
`.vscode/settings.json` file.

This will enable the linter extension to check your files as you are typing.

Not all linters have a VSCode extension, and will have to be run locally.

## Running Locally

There should be a `lint.sh` script in the `bin` directory. (Some projects
will still have a `commands` directory - this is deprecated.) Add the
appropriate Docker command(s) to the `lint.sh` script.

Run the `bin/lint.sh` script from the top-level of the project to have it
check all files with all linters.

You should run the `lint.sh` script (at a minimum) before pushing changes
to GitLab, so that you can avoid having your pipeline fail due to linting
errors.

## Running In Pipelines

Every project should have a `.gitlab-ci.yml` file to set up the project's
pipeline. The pipeline will perform build, test, and release operations, as
well as run the linters whenever changes are pushed to GitLab.

Read the [Developer Documentation on Pipelines](./Pipelines.md) for
information on how to configure a project's pipeline including enabling the
appropriate linters.

If one of the linters fails in the pipeline, the entire pipeline will fail,
and corrections will have to be made and pushed before you will be able to
merge the changes in your branch into `main`.

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
