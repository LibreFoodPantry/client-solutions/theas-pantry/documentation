# Thea's Pantry Developer Documentation

## Community Standards

* [Code of Conduct](../CODE_OF_CONDUCT.md) - All members of our community
  are expected to adhere to our Code of Conduct while participating in
  our community.
* [Developer Certificate of Origin](../DCO-1.1.txt) - All developers agree
to this when making contributions to Thea's Pantry projects.
* [Inclusive Language](./InclusiveLanguage.md) - Let's make our community
  welcoming to all who would like learn along with us and contribute. One
  way to do this is by being careful with the language we ue in our
  documentation and communications.
* [Definition of Done](./DefinitionOfDone.md) - Expectations for the quality
  of work to be contributed to our projects.

## Tools and Processes

* [Workflow](./Workflow.md) - The workflow to be used by developers.
* [Release Process](./ReleaseProcess.md) - Release process for microservices.
* [Pipelines](./Pipelines.md)
* [Linting](./Linting.md)
* [Conventional Commits](./ConventionalCommits.md)
* [Development Environment](./DevelopmentEnvironment.md)
* [Development vs. Production](./DevelopmentProduction.md)

## Design Documents

* [Architecture](./Architecture.md) - A high-level design for the pieces of
the software system and how they interact with each other.
* [ID Card Scanner](./IdCardScanner.md) - A design for using a magnetic
  card-swipe reader to enter WSU IDs either manually or from OneCards.
* [Technology](./Technology.md) - A listing of the tools and frameworks we
have decided to use to build and deploy the software system.
* [User Stories](./UserStories.md) - Stories about how the intended users
of the software currently work, used to guide the design of the software.
* [Wireframes](./Wireframes.md) - Wireframes designs for frontend GUIs.

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
