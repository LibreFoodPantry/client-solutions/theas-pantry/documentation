# Thea's Pantry Release Process

Uses [`semantic-release`](https://github.com/semantic-release/semantic-release)
to perform continuous delivery. Every time a merge request is approved, the new
version will have the version number increased based on the Convention Commit
message and the [Semantic Versioning Specification](http://semver.org/), and
the artifact (Docker image) will be pushed to the container repository.

APIs will also be versioned using `semantic-release`.

1. The GitLab CI pipeline using `semantic-release` is triggered by a commit
to the `main` branch.
2. `semantic-release` tags the Docker image and pushes it to the GitLab
container registry for the project.

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed under
the Creative Commons Attribution-ShareAlike 4.0 International License. To
view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
