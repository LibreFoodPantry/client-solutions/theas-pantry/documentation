# Inclusive Language

<!-- alex disable white gals-man -->
Computing, as a field, has a continuing problem with inclusion and
diversity. Many people with identities that are not a white, cis-gender,
heteronormative, neurotypical, non-disabled, Christian, young, man do not
feel welcome and included in the field.
<!-- alex enable white gals-man -->

This is a problem because many people who want to be in the field, who
would bring diverse perspectives, skills, and ideas to projects either do
not join the field, or leave due to not feeling like they belong or are
valued. Because of this, the tools, products, and data we produce are not
as good or as useful as they could be for a variety of people.

Often one of the first places someone encounters a feeling of not belonging
or not being welcomed is in documentation for projects and tools that are
used and built by practitioners in the community. By being thoughtful and
careful in how we write we can make sure we do not turn people away before
they even join our communities.

There are more and more guides being produced to help us write in inclusive
ways, but we are not as used to re-reading and editing our work for
inclusive language as we are for spelling and grammar.

In the same way that spell-checkers and grammar-checkers help us in our
writing, there now tools that can help with writing in an inclusive way.
The Thea's Pantry project is using the Alex linter, which is designed to
"catch insensitive, inconsiderate writing".

<!-- cSpell:disable polarising-->
> *Whether your own or someone else’s writing, **alex** helps you find
gender favouring, polarising, race related, religion inconsiderate, or
other **unequal** phrasing.*
>
> [https://alexjs.com/](https://alexjs.com/)
<!-- cSpell:enable -->

The Alex linter should be in the pipeline for each repository in the Thea's
Pantry project. It should also be in the `bin/lint.sh` script in each
repository. Unfortunately, the VSCode extension for Alex is not yet
available for GitPod, so we must run in from the command line.

And, as we know to sometimes ignore the warnings of spelling and
grammar checkers because they sometimes misinterpret our writing, we need
to be able to evaluate the warnings that inclusive writing tools produce,
to determine if their suggestions are valid.

You can run Alex with the `--why` flag for hints as to why a word may be
problematic:

<!-- markdownlint-disable MD013 -->
```bash
docker run -v "${PWD}":/workdir -w /workdir \
    registry.gitlab.com/librefoodpantry/common-services/tools/linters/alexjs:latest \
    alex . --why
```
<!-- markdownlint-enable MD013 -->

If you determine that a flagged word is not problematic, you can use an
HTML comment to have Alex ignore this instance. See
[Control](https://github.com/get-alex/alex#control)
in the documentation.

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
