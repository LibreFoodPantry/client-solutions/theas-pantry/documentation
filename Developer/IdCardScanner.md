# ID card scanner

Design for using a magnetic card-swipe reader to enter WSU IDs from
OneCards. This allows a text field to accept IDs either manually through
keyboard entry, or by using the swipe reader with an ID card.

This has been implemented but may not be integrated into all appropriate
frontends yet.

```plantuml
hide empty description
state Empty_Field
state Guard_Characters_Before : Accept ;0000 /emit nothing to field
state 7_Digits : Accept 7 digits / emit only digits to field
state Guard_Characters_After : Accept 0000 / emit nothing to field

Empty_Field --> Guard_Characters_Before : ; entered
Empty_Field --> 7_Digits : digit entered
Guard_Characters_Before --> 7_Digits 
7_Digits --> Guard_Characters_After
7_Digits --> Empty_Field : Enter key or\nsubmit button
Guard_Characters_After --> Empty_Field : Enter key or\nsubmit button
```

```plantuml
state 7_Digits {
[*] --> 1_Digit : 0-9 entered / emit digit to field
1_Digit --> 2_Digits : 0-9 entered / emit digit to field
1_Digit --> 1_Digit : anything other than 0-9 / emit nothing to field
2_Digits --> 3_Digits : 0-9 entered / emit digit to field
2_Digits --> 2_Digits : anything other than 0-9 / emit nothing to field
3_Digits --> 4_Digits : 0-9 entered / emit digit to field
3_Digits --> 3_Digits : anything other than 0-9 / emit nothing to field
4_Digits --> 5_Digits : 0-9 entered / emit digit to field
4_Digits --> 4_Digits : anything other than 0-9 / emit nothing to field
5_Digits --> 6_Digits : 0-9 entered / emit digit to field
5_Digits --> 5_Digits : anything other than 0-9 / emit nothing to field
6_Digits --> [*] : 0-9 entered / emit digit to field
6_Digits --> 6_Digits : anything other than 0-9 / emit nothing to field
}
```

```plantuml
state Guard_Characters_Before {
    state semicolon <<entryPoint>>
    state success <<exitPoint>>
    state failure <<exitPoint>>
    semicolon --> 1_Zero : 0 entered / emit nothing to field
    semicolon --> failure : anything other than 0 / emit nothing to field
    1_Zero --> 2_Zeros : 0 entered / emit nothing to field
    1_Zero --> failure : anything other than 0 / emit nothing to field
    2_Zeros --> 3_Zeros : 0 entered / emit nothing to field
    2_Zeros --> failure : anything other than 0 / emit nothing to field
    3_Zeros --> success : 0 entered / emit nothing to field
    3_Zeros --> failure : anything other than 0 / emit nothing to field
}
success --> 7_Digit
failure --> Empty_Field
```

```plantuml
state Guard_Characters_After {
1_Zero_After --> 2_Zeros_After : 0 entered / emit nothing to field
2_Zeros_After --> 3_Zeros_After : 0 entered / emit nothing to field
3_Zeros_After --> 4_Zeros_After : 0 entered / emit nothing to field
4_Zeros_After --> Empty_Field : Enter key
}
```

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
