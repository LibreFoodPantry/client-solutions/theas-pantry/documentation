# Technology

## Decided

* REST API Specification: [OpenAPI v3.0.3](http://spec.openapis.org/oas/v3.0.3)
* Frontend: [Vue.js](https://vuejs.org/)
* Backend: [Express](https://expressjs.com/)
* Data Persistence: [MongoDB](https://www.mongodb.com/)
* Containerization: [Docker](https://www.docker.com/)
* Message Queuing/Events Service: [RabbitMQ](https://www.rabbitmq.com/)
* Version Control: [Git](https://git-scm.com/)
* DevOps Platform: [GitLab](https://about.gitlab.com/)

## Evaluating

* Container Orchestration: [Kubernetes](https://kubernetes.io/)
* Identity and Access Management: [Keycloak](https://www.keycloak.org/)

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
