# Thea's Pantry Customer Documentation

* [November2020TheasPantryReport.pdf](./November2020TheasPantryReport.pdf) -
Example of the report generated for the Worcester County Food Bank
* [TheasPantryLogEntry.pdf](./TheasPantryLogEntry.pdf) - Example of the form
used to collect information about guests

---
Copyright &copy; 2024 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).
